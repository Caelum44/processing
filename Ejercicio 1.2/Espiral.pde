public class Espiral {
  private int X;
  private int Y;
  private int H;
  private int W;
  private boolean dirX;
  private boolean dirY;
  private int Speed;
  private int ColorRed;
  private int ColorBlue;
  private int ColorGreen;

  public Espiral(int x, int y, int h, int w, int speed, int colorRed, int colorBlue, int colorGreen) {
    this.X = x;
    this.Y = y;
    this.H = h;
    this.W = w;
    this.Speed = speed;
    this.ColorRed = colorRed;
    this.ColorBlue = colorBlue;
    this.ColorGreen = colorGreen;
    flip();
  }

  public int getX() {
    return this.X;
  }

  public int getY() {
    return this.Y;
  }

  public int getH() {
    return this.H;
  }

  public int getW() {
    return this.W;
  }

  public void setX(int x) {
    this.X = x;
  }

  public void setY(int y) {
    this.Y = y;
  }

  public void display() {
    fill(255);
    stroke(this.ColorRed, this.ColorBlue, this.ColorGreen);
    ellipse(this.X, this.Y, this.H, this.W);
  }

  public void flip() {
    this.dirX = int(random(2)) == 1;
    this.dirY = int(random(2)) == 1;
  }

  public void move() {
    if (this.dirX) {
      this.X += this.Speed;
    } else {
      this.X -= this.Speed;
    }

    if (this.dirY) {
      this.Y += this.Speed;
    } else {
      this.Y -= this.Speed;
    }
  }

  public void die() {
    fill(this.ColorRed, this.ColorBlue, this.ColorGreen);
    stroke(this.ColorRed, this.ColorBlue, this.ColorGreen);
    ellipse(this.X, this.Y, this.H, this.W);
  }
}

