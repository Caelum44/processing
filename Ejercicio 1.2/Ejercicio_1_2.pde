// Public variables
// *
// Private variables
private ArrayList<Espiral> Espirales;
private int timer, startTimer;
private int espiralH, espiralW, espiralSpeed, espiralRed, espiralBlue, espiralGreen;
private int mapX, mapY;
private int bgColor;
private int moveChance, flipSpeed, birthSpeed;

void setup() {
  // #######################
  // #### Configuración ####
  // 1-100
  birthSpeed = 50;
  // 1-10;
  moveChance = 9;
  // 1-20
  flipSpeed = 10;
  // 1-5
  espiralSpeed = 1;
  // #######################
  // #######################

  // Entorno
  bgColor = 255;
  mapX = 500;
  mapY = 500;

  // Timers
  timer = 0;
  startTimer = 50;

  // Espiral
  espiralH = 10;
  espiralW = 10;

  // Setters
  size(mapX, mapY);
  background(bgColor);
  Espirales = new ArrayList<Espiral>();
}

void draw() {
  if (mousePressed) {
    background(bgColor);
    timer = startTimer;
    Espirales = new ArrayList<Espiral>();
  }

  if (timer == 0) {
    timer = int(random(1, 100));
    int newX = int(random(espiralW/2, mapX-espiralW/2));
    int newY = int(random(espiralH/2, mapY-espiralH/2));
    espiralRed = int(random(200));
    espiralBlue = int(random(200));
    espiralGreen = int(random(200));
    Espirales.add(new Espiral(newX, newY, espiralH, espiralW, espiralSpeed, espiralRed, espiralBlue, espiralGreen));
  } else {
    timer -= 1;
  }

  for (int i=Espirales.size () - 1; i>=0; i--) {
    Espiral e = Espirales.get(i);

    if (int(random(21-flipSpeed)) == 0) {
      e.flip();
    }
    if (int(random(11-moveChance)) == 0) {
      e.move();
    }
    if (checkEspiralPosition(e)) {
      e.display();
    } else {
      e.die();
      Espirales.remove(i);
    }
  }
}

private boolean checkEspiralPosition(Espiral e) {
  int x, y, h, w;
  x = e.getX();
  y = e.getY();
  h = e.getH();
  w = e.getW();
  if ((x-w/2) <= 0) {
    return false;
  } else if ((x+w/2) >= mapX) {
    return false;
  } else if ((y-h/2) <= 0) {
    return false;
  } else if ((y+h/2) >= mapY) {
    return false;
  } else {
    return true;
  }
}

